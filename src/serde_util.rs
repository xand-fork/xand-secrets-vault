use secrecy::{ExposeSecret, Secret};
use serde::Serializer;

/// Structs can use this util method with serde's `serialize_with` to allow for serializing the Secret
pub(crate) fn serialize_secret<S>(x: &Secret<String>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(x.expose_secret())
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Serialize, Deserialize)]
    struct TestSecretContainer {
        #[serde(serialize_with = "serialize_secret")]
        pub secret: Secret<String>,
    }

    #[test]
    fn serialize_secret_returns_expected_yaml() {
        // Given
        let s = TestSecretContainer {
            secret: Secret::new("somesecret".to_string()),
        };

        // When
        let serialized = serde_yaml::to_string(&s).unwrap();

        // Then
        let expected = r#"---
secret: somesecret
"#;

        assert_eq!(serialized, expected);
    }

    #[test]
    fn serialize_secret_serde_roundtrip() {
        // Given
        let s = TestSecretContainer {
            secret: Secret::new("somesecret".to_string()),
        };
        let serialized = serde_yaml::to_string(&s).unwrap();

        // When
        let deserialized: TestSecretContainer = serde_yaml::from_str(&serialized).unwrap();

        // Then
        assert_eq!(deserialized.secret.expose_secret(), "somesecret");
    }
}
